package com.vigion.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by maria on 02/02/2016.
 */
public class SQLiteLocalConnector extends SQLiteOpenHelper{
    //Base de Dados: Nome.db3
    private static final String DATABASE = "Escola.db3";

    //Versão da BD Controla os onUpgrade()
    private static final int VERSION = 1;

    //-------------------------------------------------------------------------------------
    // metodos da classe super: Contrutor(), onCreate() e onUpdate()
    //-------------------------------------------------------------------------------------

    public SQLiteLocalConnector(Context context) {
        super(context, DATABASE, null, VERSION);
        Log.i("LOG:", "SQLiteLocalConnector - Contrutor() COMPLETO");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
