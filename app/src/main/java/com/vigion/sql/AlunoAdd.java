package com.vigion.sql;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AlunoAdd extends AppCompatActivity {

    //Views
    private EditText editTextNome;
    private EditText editTextUserName;
    private EditText editTextPassword;

    private Button buttonAdd;
    private Button buttonLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Aquisição do controlo de objetos gráficos
        editTextNome = (EditText) findViewById(R.id.editTextNome);
        editTextUserName = (EditText) findViewById(R.id.editTextUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        //Botões e Listeners
        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonLista = (Button) findViewById(R.id.buttonLista);
        buttonLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(),AlunosListView.class));
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAluno();
                Log.i("BTN ADD", "CLicou no botão");
            }
        });
    }

    //Adiciona um aluno
    private void addAluno(){
        //Recolha dos dados, inseridos pelo ser
        final String nome = editTextNome.getText().toString().trim();
        final String username = editTextUserName.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        if(nome.isEmpty() || username.isEmpty() || password.isEmpty()){
            Toast.makeText(AlunoAdd.this, "Campos sem dados", Toast.LENGTH_LONG).show();
        }
        else {
        }
    }
}
