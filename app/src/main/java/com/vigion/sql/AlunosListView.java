package com.vigion.sql;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;

public class AlunosListView extends AppCompatActivity {

    private ListView listView;              //Objeto java para controlo do objeto gráfico ListView
    private String itemNproc ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alunos_list_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //listView = (ListView) findViewById(R.id.listView);
        //listView.setOnItemClickListener(new ListView.OnItemClickListener() {

        //@Override
        //public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Obtém o item com as duas strings1 o 1º é nProc, 2º é o nome
        //HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

        //Extraiu o nProc do item
        //String itemNproc = map.get(Config.TAG_NPROC).toString();

        //Prepara a chamada da activity para exibir os dados do item clicado
        Intent intent = new Intent(AlunosListView.this, AlunoEdit.class);
        intent.putExtra("nProc",itemNproc);     //adiciona-lhe o nProc do registo a exibir

        showList();
    }

    private void showList() {
        Toast.makeText(getApplicationContext(), "Por fazer", Toast.LENGTH_LONG).show();
    }


}