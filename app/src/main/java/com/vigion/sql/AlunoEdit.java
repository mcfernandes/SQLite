package com.vigion.sql;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class AlunoEdit extends AppCompatActivity {

    //objetos para controlo dos objetos graficos
    private TextView textViewNproc;
    private EditText editTextNome, editTextUserName, editTextPassword;

    private String nProc;       //recebe valor da AlunoListView

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //aquisição do controlo dos objetos graficos
        textViewNproc = (TextView) findViewById(R.id.textViewNproc);
        editTextNome = (EditText) findViewById(R.id.editTextNome);
        editTextUserName = (EditText) findViewById(R.id.editTextUserName);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        //extração do atributo passado pela activity da ListView
        Intent intent = getIntent();
        nProc = intent.getStringExtra("nProc");
        textViewNproc.setText(textViewNproc.getText() + nProc);       //atualiza a TextViewNproc com nProc
        Log.i("LOG:", "AlunoEdit - onCreate() " + nProc);
    }

    private void getAluno() {
        Toast.makeText(getApplicationContext(), "Por fazer", Toast.LENGTH_LONG).show();
    }

    private void updateAluno() {

        final String nome = editTextNome.getText().toString().trim();
        final String userName = editTextUserName.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        }

    private void deleteAluno() {
        Toast.makeText(getApplicationContext(), "Por fazer", Toast.LENGTH_LONG).show();
    }

    private void confirmDeleteAluno() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Confirma e eliminação?");
        alertDialogBuilder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                deleteAluno();
                startActivity(new Intent(AlunoEdit.this, AlunosListView.class));
            }
        });
        alertDialogBuilder.setNegativeButton("Nao", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
